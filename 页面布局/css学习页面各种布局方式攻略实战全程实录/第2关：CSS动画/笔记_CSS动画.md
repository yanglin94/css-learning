

一、浏览器私有前缀

    CSS3是CSS（层叠样式表）技术的升级版本。CSS3完全向后兼容，不必改变现有的设计，浏览器将永远支持CSS2。W3C的CSS3规范仍在开发。
但是，许多新的CSS3属性已在现代浏览器使用。

(一)浏览器内核以及其前缀
    CSS标准中各个属性都要经历从草案到推荐的过程，css3中的属性进展都不一样，浏览器厂商在标准尚未明确情况下提前支持会有风险，
浏览器厂商对新属性的支持情况也不同，所以会加厂商前缀加以区分。如果某个属性已经从草案变为了或接近推荐方案，并且厂商已经完全实现了推荐属性，
那就不用加厂商前缀。如border-radius已经很成熟，不用加前缀。


	根据不同的浏览器内核，css前缀会有不同。最基本的浏览器内核有如下四种，其它的内核都是基于此四种进行再研发的。

1.Gecko内核      前缀为 -moz-   火狐浏览器

2.Webkit内核    前缀为 -webkit-   也叫谷歌内核，chrome浏览器最先开发使用，safari浏览器也使用  
    该内核。国内很多浏览器也使用了webkit内核，如360极速、世界之窗、猎豹等。

3.Trident内核    前缀为 -ms-  也称IE内核

4.Presto内核      前缀 -o-   目前只有opera采用



二、圆角及阴影

1、圆角border-radius
	语法：
	border-radius：value；四个角
	border-radius：value value；左上右下、右上左下
	border-radius：value value value value； 代表设置对象左上角、右上角、右下角、左下角

​	顺时针设置

2、盒阴影box-shadow
	语法：box-shadow: h-shadow v-shadow blur spread color inset;

![阴影](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\盒阴影.png)

​	盒阴影

HTML文档：

```html
<div class="shadow"></div>
```

CSS样式：

 .shadow{

​      width: 300px;

​      height: 100px;

​      background-color: red;

​      box-shadow: 10px 10px 5px 3px darkred;

​    }

![阴影效果](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\盒阴影效果.png)

3、文字阴影

​	语法：text-shadow: h-shadow v-shadow blur color;

![文字阴影](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\文字阴影.png)

html

```html
<div class="text1">学习猿地</div>
```

CSS

```css
.text1{
	font-size: 50px; 
    font-weight: bold; 
    color: palegreen;
    text-shadow: 10px -5px 5px #ccc ;
}
```

![文字阴影效果](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\文字阴影效果.png)

三、CSS 渐变

​		CSS3 渐变（gradients）可以让你在两个或多个指定的颜色之间显示平稳的过渡。以前，你必须使用图像来实现这些效果。现在，使用 CSS3 渐变（gradients），通过代码来实现渐变可以减少请求和节约带宽。



CSS3 定义了两种类型的渐变（gradients）：

​		线性渐变（Linear Gradients）

​		径向渐变（Radial Gradients）

![渐变效果图](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\渐变效果图.png)

CSS3 定义了两种类型的渐变（gradients）：

1.线性渐变（Linear Gradients）- 向下/向上/向左/向右/对角方向

 		background: linear-gradient(direction, color-stop1, color-stop2, ...);

2.径向渐变（Radial Gradients）- 由它们的中心定义

 		background: radial-gradient(center, shape， size, start-color, ..., last-color);

​	 	默认情况下，渐变的中心是 center（表示在中心点），渐变的形状是 ellipse（表示椭圆形）

​	 	它可以是值 circle 或 ellipse。其中，circle 表示圆形，ellipse 表示椭圆形



四、转换Transform

​		CSS3中的转换允许我们对元素进行旋转、缩放、移动或倾斜。它为分2D转换 或 3D 转换。

 		在css2时代，如果要做一些图片转换角度，都依赖于图片、Flash或JavaScript才能完成。但是现在借助CSS3就可以轻松倾斜、缩放、移动以及翻转元素。通过CSS变形，可以让元素生成静态视觉效果，但也可以很容易结合CSS3的transition和动画的keyframe产生一些动画效果。

（一）转换Transform 2D属性

​		 通常的属性包含了属性名和属性值，而CSS3的transform属性是用函数来定义的。Transform 2D函数包括了translate()、scale()、rotate()和skew()。

书写格式:

​	 transform:函数名(x轴值，y轴值);

转换的效果：

![2D转换效果](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\2D转换效果.png)



1.translate()函数

​	 translate()方法，根据左(X轴)和顶部(Y轴)位置给定的参数，从当前元素位置移动。

​	接受CSS的标准度量单位（px）

　 translate(x,y)：转换，沿着X和Y轴移动元素。

2.rotate()

​	 通过 rotate() 方法，元素顺时针旋转给定的角度。允许负值，元素将逆时针旋转。它以deg为单位，代表了旋转的角度。

3.scale()

 	通过值把宽和高转换为原始尺寸的n倍，接受两个参数，前面的为宽，后面的为高。

​	可取值：

​		默认值为1

​		缩小：0-1之间的数

​		放大：大于1的数

4.skew()

 		根据水平轴和垂直轴翻转，接受两个或一个值，

​		两个值时前面为水平，后面为垂直的角度 ，一个值只是水平轴的角度。

​		此函数是指元素的倾斜角度。

（二）转换Transform 3D 的属性

​		Transform 3D常用函数有

![3D转换](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\3D转换.png)



五、过度Transition

（一）什么是过渡

​	使用css的属性值在一段时间内平滑的过渡

​	比如，鼠标悬停后，背景色在1s内，由白色平滑的过渡到红色

​	1）指定四个要素：

​	过渡属性，如background、color等

​	过渡所需时间

​	过渡函数，即过渡的速度、方式等

​	过渡延迟时间，表示开始执行的时间

​	 2）触发过渡

​	通过用户的行为触发，如点击、悬浮等

1、过渡属性

transition-property: none |  all |  property;

多个属性用逗号隔开

可设置过渡的属性：

​	颜色属性

​	取值为数值的属性

​	转换属性

​	渐变属性

​	阴影属性

2、过渡时间

​	transition-duration: s  |  ms;

​	默认值为0，意味着不会有效果，所以必须设置transition-duration属性

3、过渡函数

​	transition-timing-function: ;

​	取值：

​		ease：默认值，规定慢速开始，然后变快，然后慢速结束的过渡效果

​		linear：匀速

​		ease-in：规定以慢速开始，加速效果

​		ease-out：规定以慢速结束，减速效果

​		ease-in-out：规定以慢速开始和结束，先加速后减速效果

4.过渡延迟

​	transition-delay:  s  |  ms;

​	改变元素属性值后多长时间开始执行过渡效果

5.简写属性 transition

​	transition属性是一个简写属性，用于设置四个过渡属性

​	语法：transition:property duration timing-function delay;

```css
       #box{
            width: 200px;
            height: 200px;
            background-color: #1fb57b;
            transition: background 4s linear 1s;
        }
        #box:hover{
            background-color: red;
       }
```



六、动画animation

​		过渡属性只能模拟动画效果

​		animation属性可以制作类似Flash动画

​		通过关键帧控制动画的每一步

​		使元素从一种样式逐渐变化为另一种样式

​		实现复杂的动画效果

1、@keyframes

​	（1）作用：用于声明动画，指定关键帧

​		帧，用于分解动画动作

​		每个帧代表某个时间点

​		定义每个帧上的动作

​	（2）@keyframes的语法：

```css
@keyframes name  {
    from|0%{
        css样式
    }
    percent{
        css样式
    }
    to|100%{
        css样式
    }
 }

```

![动画](D:\learn\cssLearning\页面布局\css学习页面各种布局方式攻略实战全程实录\第2关：CSS动画\动画.png)



2、animation属性

​		animation属性用于控制动画

​		调用由@keyframes定义的动画

​		设置动画属性，如时间、次数等

​		animation属性是一个简写属性

​		语法为：animation:name duration timing-function delay iteration-count direction;



3.动画子属性

​		animation-name: ; 调用动画，规定需要和keyframes的名字一致

​		animation-duration: s  |  ms; 动画完成一个周期所需要的时间

​		animation-timing-function: ; 规定动画的速度变化类型

​		animation-delay:s|ms ; 播放之前的延迟时间

​		（1）animation-iteration-count: 数值|infinite; 播放次数

​					infinite表示无限次播放

​		（2）animation-direction: normal|alternate; 动画播放方向

​					normal为默认值，表示正常播放

​					alternate表示轮流播放，即动画会在奇数次正常播放，而在偶数次向后播放

​		（3）animation-fill-mode: forwards; 动画停在最后一帧

​					 默认值为none

​		（4）animation-play-state: paused  |  running; 属性规定动画正在运行还是暂停

​					 默认值为running